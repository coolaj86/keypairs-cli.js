set -e
echo ""
echo ""
node bin/keypairs-cli.js gen > ./fixture.ec.jwk.json 2> ./fixture.ec.jwk.pub.json
node bin/keypairs-cli.js ./fixture.ec.jwk.json pkcs8 > fixture.ec.pem
node bin/keypairs-cli.js ./fixture.ec.pem > ./fixture.ec.jwk.json

node bin/keypairs-cli.js gen rsa > ./fixture.rsa.jwk.json 2> ./fixture.rsa.jwk.pub.json
node bin/keypairs-cli.js ./fixture.rsa.jwk.json pkcs1 > fixture.rsa.pem
node bin/keypairs-cli.js ./fixture.rsa.pem > ./fixture.rsa.jwk.json

node bin/keypairs-cli.js ./fixture.ec.jwk.json sign https://localhost:4080 '{}' 3d > works.txt;
node bin/keypairs-cli.js ./fixture.ec.jwk.pub.json decode works.txt
#node bin/keypairs-cli.js ./fixture.ec.jwk.pub.json decode works.txt
node bin/keypairs-cli.js ./fixture.ec.jwk.json sign https://localhost:4080 '{}' 3d > works.txt;
