# Keypairs CLI

The most useful and easy-to-use crypto cli on the planet
(because `openssl` is confusing).

* [x] Universal Standards-based Crypto Support:
  * [x] RSA (2048, 3072, 4096, 8192)
  * [x] EC (NIST ECDSA) P-256 (prime256v1, secp256r1), P-384 (secp384r1)
* [x] Supported Encodings: PEM, JSON
* [x] Private Key Formats: PKCS1, SEC1, PKCS8, JWK, OpenSSH
* [x] Public Key Formats: PKCS1, PKIX (SPKI), SSH
* [x] Create JWT tokens
* [x] Sign JWT/JWS claims/tokens/payloads
* [x] Decode JWTs (without verifying)
* [x] Verify JWT/JWS tokens/json (by fetching public key)

# Install

You must have [node.js](https://nodejs.org) installed.

```bash
npm install --global keypairs-cli
```

# Usage

Guess and check.

The keypairs CLI is pretty fuzzy. **If you just type at it, it'll probably work.**

That said, the fuzzy behavior is _not_ API-stable and is subject to change,
so you should only script to the documented syntax. ;)

# Overview

* Generate: `keypairs gen`
* Convert: `keypairs ./priv.pem`
* Sign: `keypairs sign ./priv.pem https://example.com/ '{"sub":"jon@example.com"}'`
* Verify: `keypairs verify 'xxxxx.yyyyy.zzzzz'`
* Decode: `keypairs decode 'xxxxx.yyyyy.zzzzz'`
* Debug: prefix any option with `debug` such as `keypairs debug gen pem key.pem jwk pub.json`

## Generate a New Key

No arguments - generates a universally compatible key of more-than-sufficient entropy.

```bash
keypairs gen
```

Generate an ecdsa key:

```bash
keypairs gen ec P-256
```

Generate an RSA key:

```bash
keypairs gen rsa 2048
```

## Parse/Convert an existing key

```bash
keypairs ./priv.pem
```

```bash
keypairs '{"kty":"EC",...}'
```

```bash
keypairs ./priv.jwk.json
```

**Syntax**: `keypairs <in> [priv-out opts...] [pub-out opts...]`

```bash
keypairs <inkey> [[encoding|scheme] [priv-out]] [[encoding|scheme] [pub-out]] [public|private]
```

**Note**: If you specify a private _and_ a public key, and you want to specify the schema/encoding
of the public key, you must also specify the scheme and encoding of the public key. Order matters.
Private keys come first.

JWK Keypair to PEM-encoded Private and Public keys:

```bash
keypairs ./priv.json pem pkcs1 ./priv.pem pem spki ./pub.pem
keypairs ./priv.json pem ./priv.pem ssh ./pub.json
keypairs ./priv.json pkcs8 ./priv.pem spki ./pub.json
```

PEM Keypair to JSON-encoded JWK (Public Key Only):

```bash
keypairs ./priv.pem jwk ./priv.pem public
keypairs ./priv.pem json ./priv.pem public
```

Generic PEM to JWK:

```bash
keypairs priv.pem priv.jwk.json
```

```bash
keypairs priv.pem priv.jwk.json pub.jwk.json
```

```bash
keypairs priv.pem pub.jwk.json public
```

```bash
# fails if the input is public
keypairs priv.pem priv.jwk.json private
```


Generic JWK to PEM:

```bash
keypairs '{"kty":"EC",...}' priv.pem
```

```bash
keypairs priv.json priv.pem
```

## Sign a Token (JWT)

<!-- or Payload (JWS) -->

**Syntax**:

```bash
keypairs [key] sign [issuer url] <claims> [exp] [nbf]
```

_Note: The issuer url can be omitted if it's already included among the claims._

Example:

```bash
keypairs ./priv.pem sign https://example.com/ '{"sub":"jon@example.com"}' 1h -5m
```

```bash
keypairs '{"kty":"EC",...}' sign https://example.com/ '{"sub":"jon@example.com"}' 1h -5m
```

## Verify a JWT (Token)

<!-- or JWS (Payload) -->

Verify a JWT based on its issuer

```bash
keypairs verify 'xxx.yyy.zzz'
```

<!--
Verify using a specific key

```bash
keypairs priv.pem verify 'xxx.yyy.zzz' nofetch
```
-->
